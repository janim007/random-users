import React, {useState, useEffect} from 'react';


const UsersTable = props => {
    const _users = props.users;
    if (!_users.length) return <></>
    return <table>
        <thead>
            <tr>
                {Object.keys(_users[0]).map((i, idx)=>
                    <th className="sort-down" key={idx} onClick={e=>props.sort(e, i)}>{i}</th>
                )}
            </tr>
        </thead>
        <tbody>
            {_users.map((i, idx)=> {
                return <tr key={idx}>
                    {Object.values(i).map((item, idx)=> {
                        return <td key={idx}>{item}</td>
                    })}
                </tr>
            })}
        </tbody>
    </table>
}


const Users = props => {
    const [users_lst, setUsersList] = useState([]);

    /* flatten locations array
    */
    const flatten = (obj, p) => {
        let flattened_obj = {}
         Object.keys(obj).forEach( i => {
            if (typeof obj[i] === 'object' && obj[i] !== null) {
                Object.assign(flattened_obj, flatten(obj[i], i) )
            }else {
                flattened_obj[i] = obj[i]
            }
        })
        return flattened_obj
    }

    /** Array.sort callback,
        params: i: item to sort against
     */
    const sortFunc = i => {
        return (a,b) => {
            return (a[i] < b[i]) ? -1 : (a[i] > b[i]) ? 1 : 0;
        }
    }

    /*
    * sort location columns
    * params:
    * e: event
    * i: item to be sorted
    */
    const sort = async (e, i) => {

        const _users = [...users_lst];
        const sorted = e.target.classList.contains('sort-up')
        if (sorted) {
            // if it's already sorted and user clicked again
            // they might be looking for a reverse results
            _users.sort(sortFunc(i)).reverse()
            e.target.classList.replace('sort-up', 'sort-down')
        }else {
            // reset all other icons
            document.querySelectorAll(".users-list > table > thead > tr th[class='sort-up']").forEach(i=>{
                i.classList.replace('sort-up', 'sort-down')
            })
            _users.sort(sortFunc(i))
            e.target.classList.replace('sort-down', 'sort-up')
        }
        setUsersList(_users)
    }

    useEffect( async () => {
        // fetch users data
        let http_resp = await fetch("https://randomuser.me/api/?results=20");
        if (!http_resp.ok) throw new Error("could not load users");
        http_resp = await http_resp.json();
        // filter location field
        http_resp = http_resp.results.map(i=>i.location)

        // flatten user location
        let flattened = []
        for (let location of http_resp) {
             flattened.push(flatten(location, null))
        }

        setUsersList(flattened)
    }, [])

    return (<div className='users-list'>
        <UsersTable users={users_lst} sort={sort} />
    </div>)
}

export default Users;
