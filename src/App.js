import React from "react";
import Header from './Header';
import Users from './Users';

function App() {
  return (<>
    <div className="App">
      <Header />
      <Users />
    </div>
  </>);
}

export default App;
